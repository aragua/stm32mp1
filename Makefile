
DL_DIR=${PWD}/downloads
SRC_DIR=${PWD}/src
BUILD_DIR=${PWD}/build

all: image

YOCTO_DIR=${PWD}/yocto
YOCTO_BUILD_DIR=${YOCTO_DIR}/buildweston
YOCTO_MACHINE=stm32mp1-disco
YOCTO_DISTRO=openstlinux-weston

${YOCTO_DIR}/.repo:
	mkdir -p ${YOCTO_DIR}
	cd ${YOCTO_DIR}
	repo init -u https://github.com/koansoftware/koan-stm32mp-bsp-repo -b thud
	repo sync
	cd -

${YOCTO_BUILD_DIR}: ${YOCTO_DIR}/.repo
	MACHINE=${YOCTO_MACHINE} DISTRO=${YOCTO_DISTRO} source setup-environment buildweston

TOOLCHAIN_DIR=${BUILD_DIR}/toolchain
TOOLCHAIN_ENV=${TOOLCHAIN_DIR}/environment-setup-cortexa7t2hf-neon-vfpv4-openstlinux_weston-linux-gnueabi

${DL_DIR}/toolchain.sh: ${YOCTO_BUILD_DIR}
	@echo ++++++++++          Build $@          ++++++++++
	./scripts/build-toolchain ${YOCTO_DIR}
	cp ${YOCTO_BUILD_DIR}/tmp-glibc/deploy/sdk/meta-toolchain-*-snapshot.sh $@
	mkdir -p ${TOOLCHAIN_DIR}
	${DL_DIR}/toolchain.sh -y -d ${TOOLCHAIN_DIR}

${TOOLCHAIN_ENV}: ${DL_DIR}/toolchain.sh

toolchain: ${TOOLCHAIN_ENV}

image: ${YOCTO_BUILD_DIR}
	@echo ++++++++++          Build $@          ++++++++++
	./scripts/build-yocto ${YOCTO_DIR}

KERNEL_RELEASE=v5.x
KERNEL_VERSION=5.4.2
KERNEL_NAME=linux-${KERNEL_VERSION}
KERNEL_TARBALL=${KERNEL_NAME}.tar.xz
KERNEL_URL=https://cdn.kernel.org/pub/linux/kernel/${KERNEL_RELEASE}/${KERNEL_TARBALL}

KERNEL_SRC_DIR=${SRC_DIR}/${KERNEL_NAME}
KERNEL_BUILD_DIR=${BUILD_DIR}/${KERNEL_NAME}

${DL_DIR}/${KERNEL_TARBALL}:
	mkdir -p ${DL_DIR}
	wget -O $@ ${KERNEL_URL}

${SRC_RELEASE}: ${DL_DIR}/${KERNEL_TARBALL}
	@echo extract $^
	mkdir -p ${SRC_DIR}
	tar xf $^ -C ${SRC_DIR}

${BUILD_RELEASE}:
	@echo ++++++++++          Build $@          ++++++++++
	mkdir -p ${BUILD_RELEASE}

${BUILD_RELEASE}/.config: ${KERNEL_SRC_DIR} ${KERNEL_BUILD_DIR}
	@echo ++++++++++          Build $@          ++++++++++
	make -C ${KERNEL_SRC_DIR} O=${KERNEL_BUILD_DIR} ARCH=arm multi_v7_defconfig

kernel: ${TOOLCHAIN_ENV} ${BUILD_RELEASE}/.config
	@echo ++++++++++          Build $@          ++++++++++
	./scripts/build-kernel ${TOOLCHAIN_ENV} ${KERNEL_SRC_DIR} ${KERNEL_BUILD_DIR} 
	./scripts/build-modules ${TOOLCHAIN_ENV} ${KERNEL_SRC_DIR} ${KERNEL_BUILD_DIR} 

.PHONY: kernel all toolchain image
